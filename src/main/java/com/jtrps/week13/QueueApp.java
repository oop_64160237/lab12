package com.jtrps.week13;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.*;

public class QueueApp extends JFrame{
    JTextField txtName;
    JLabel lblQueueList;
    JLabel lblCurrent;
    JButton btnAddQueue, btnGetQueue, btnClearQueue;
    LinkedList<String> queue;

    public QueueApp() {
        super("--- Queue App ---");
        queue = new LinkedList<>();

        this.setSize(400,300);
        
        txtName = new JTextField();
        txtName.setBounds(20, 55 , 210, 30);
        txtName.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
            }
        });

        btnAddQueue = new JButton("Add Queue");
        btnAddQueue.setBounds(245, 55, 120, 30);
        btnAddQueue.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                addQueue();
            } 
            
        });

        btnGetQueue = new JButton("Get Queue");
        btnGetQueue.setBounds(245, 85, 120, 30);
        btnGetQueue.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                getQueue();
            }
        });

        btnClearQueue = new JButton("Clear Queue");
        btnClearQueue.setBounds(245, 115, 120, 30);
        btnClearQueue.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                clearQueue();
            }
        });

        lblQueueList = new JLabel();
        lblQueueList.setBounds(20, 85, 210, 30);
        lblQueueList.setHorizontalAlignment(JLabel.LEFT);

        lblCurrent = new JLabel("Queue is empty");
        lblCurrent.setBounds(20, 160, 210, 30);
        lblCurrent.setFont(new Font("Angsna New", Font.PLAIN,24));
        lblCurrent.setHorizontalAlignment(JLabel.CENTER);
        
        this.add(lblCurrent);
        this.add(lblQueueList);
        this.add(btnAddQueue);
        this.add(btnGetQueue);
        this.add(btnClearQueue);
        this.add(txtName);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        showQueue();
        addQueue();

    }
    public void showQueue() {
        if (queue.isEmpty()) {
            lblQueueList.setText("Empty");
        }else{
                    lblQueueList.setText(queue.toString());
        }
    }

    public void addQueue() {
        String name = txtName.getText();
        if (name.equals("")) {
            return;
        }
        queue.add(name);
        txtName.setText("");
        showQueue();
    }

    public void getQueue() {
        if (queue.isEmpty()) {
            lblCurrent.setFont(new Font("Angsna New", Font.PLAIN,16));
            lblCurrent.setText("Error: Please add queue first");
            return;
        }
        String name = queue.remove();
        lblCurrent.setText(name);
        showQueue();
    }

    public void clearQueue() {
        queue.clear();
        lblCurrent.setText("Queue is empty");
        showQueue();
    }
    public static void main(String[] args) {
        QueueApp frame = new QueueApp();
        frame.setVisible(true);
    }
}
