package com.jtrps.week13;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class HelloMyName extends JFrame {
    JLabel lblName;
    JTextField txtName;
    JButton btnHello;
    JLabel lblHello;
    public HelloMyName() {
        super("Hello My Name");

        lblName = new JLabel("Name : ");
        lblName.setBounds(115, 55, 50, 20);
        lblName.setHorizontalAlignment(JLabel.RIGHT);

        txtName = new JTextField();
        txtName.setBounds(165, 55 , 200, 20);

        btnHello = new JButton("Click to start");
        btnHello.setBounds(115, 85, 250, 20);
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String myName = txtName.getText();
                lblHello.setText("Hello " + myName + " !!!");
            }
        });

        lblHello = new JLabel("Waiting ...");
        lblHello.setBounds(115, 115, 250, 20);
        lblHello.setHorizontalAlignment(JLabel.CENTER);

        this.add(lblHello);
        this.add(btnHello);
        this.add(txtName);
        this.add(lblName);

        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

    }

    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
        frame.setVisible(true);
    }
}
