package com.jtrps.week13;

import javax.swing.*;

class MyFrame extends JFrame{
    JButton button;
    public MyFrame() {
        this.setTitle("First Jframe");
        button = new JButton("Click");
        button.setBounds(150, 150, 100, 40);
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
        frame.setVisible(true);
    }
}
